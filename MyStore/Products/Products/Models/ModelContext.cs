﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Design;
//using System.Data.Entity.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Products.Models
{
    public partial class WebShopEntities : DbContext
    {
        public WebShopEntities(DbContextOptions<WebShopEntities> options)
            : base(options)
        {
        }

        public virtual DbSet<ExtraRef> ExtraRef { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<SYSUser> SYSUser { get; set; }
    }
}
