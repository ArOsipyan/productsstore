﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Products.Models
{
        public partial class ExtraRef
        {
            
            public string Id_User { get; set; }
            public string Id_Product { get; set; }

            [Key]
            public int Number { get; set; }

            public virtual SYSUser SYSUser { get; set; }
            public virtual Product Product { get; set; }
        }
    
}
