﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Products.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    P_Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    Extra_Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SYSUser",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true),
                    login = table.Column<string>(nullable: true),
                    IsConfirmed = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SYSUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExtraRef",
                columns: table => new
                {
                    Id_User = table.Column<string>(nullable: true),
                    Id_Product = table.Column<string>(nullable: true),
                    Number = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SYSUserId = table.Column<string>(nullable: true),
                    ProductId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtraRef", x => x.Number);
                    table.ForeignKey(
                        name: "FK_ExtraRef_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExtraRef_SYSUser_SYSUserId",
                        column: x => x.SYSUserId,
                        principalTable: "SYSUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExtraRef_ProductId",
                table: "ExtraRef",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraRef_SYSUserId",
                table: "ExtraRef",
                column: "SYSUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExtraRef");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "SYSUser");
        }
    }
}
